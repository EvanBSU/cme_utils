from os.path import join
from setuptools import setup

PACKAGES = ['cme_utils',
            'cme_utils.analyze',
            'cme_utils.plot',
            'cme_utils.manip',
            'cme_utils.job_control'
            ]

PACKAGE_DIR = {}
PACKAGE_DATA = {}


def readme():
    with open('README.md') as f:
        return f.read()


for pkg in PACKAGES:
    PACKAGE_DIR[pkg] = join(*pkg.split('.'))
setup(
    name='cme_utils',
    version='0.2',
    author='Eric Jankowski',
    author_email='ericjankowski@boisestate.edu',
    url='https://bitbucket.org/cmelab/cme_utils',
    description='Jankowski lab helper functions.',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3 :: Only',
        'Intended Audience :: Science/Research',
    ],
    packages=PACKAGES,
    package_dir=PACKAGE_DIR,
    package_data=PACKAGE_DATA,
    tests_require=[
        'pytest'
    ],
    install_requires=[
        'numpy',
        'matplotlib',
        'scipy',
        'mdtraj',
        'pillow',
        'scikit-image',
        'pandas',
        'gsd',
        'signac'
    ],
    entry_points = {
            'console_scripts': [
                'diffract=cme_utils.analyze.diffract:main',
                'peakfinder=cme_utils.analyze.peakfinder:main',
                'logplot=cme_utils.plot.logplot:main',
                'thin_gsd=cme_utils.manip.gsd_thinner:main',
                'diffjobs=cme_utils.job_control.diff_signac_jobs:main',
                'bondist=cme_utils.analyze.bond_dist:main',
                "gsd2xml=cme_utils.manip.convert_gsd_xml:main",
            ]
    },
    include_package_data=True,
    zip_safe=False
)
