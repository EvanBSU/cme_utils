import numpy as np

def n_unwrapped(positions, box):
    s = 0
    for i in range(3):
        s += np.sum(np.absolute(positions[i]) > box[i]/2.)
    return s

def plain_pbc(positions, box):
    '''Wraps positions of a single into a periodic box.
    Arguments:
        positions = numpy array (assumes just three coordinates of one particle)
        box = tuple or list, should have three elements
              assumes box goes from -L/2 to L/2.
    Returns: array of wrapped coordiantes
    '''
    p = np.copy(positions)
    for i in range(3):
        mask = np.absolute(p[i]) > box[i]/2. #TODO: Oh dear, what if the leading index is particle_id???
        if mask:
            p[i] -= np.sign(p[i])*box[i]
    if n_unwrapped(p, box) == 0:
        return p
    else:
        return plain_pbc(p, box)

def shift_pbc(positions, box):
    '''Wraps particle positions into a periodic box.
    Arguments:
        positions = numpy array
        box = numpy array of box lengths
              assumes box goes from -L/2 to L/2.
    Returns: p,image
        wrapped coordinate array and image array
    '''
    p = np.copy(positions)
    p += box/2.
    image = np.copy(p)
    image[:] /= box
    image = np.array(image, dtype=int)
    p[:] -= image[:]*box
    p[p[:, 0] < 0., 0] += box[0]
    p[p[:, 1] < 0., 1] += box[1]
    p[p[:, 2] < 0., 2] += box[2]
    p -= box/2.
    return p, image

def pbc(vec, L=50000.0):
    for i in range(len(vec)):
        d = vec[i] - L/2.0
        if d > 0.0:
            vec[i] = -L/2.0 + d
        d = -L/2.0 - vec[i]
        if d > 0.0:
            vec[i] = L/2.0 - d
    return vec

def relative_pbc(positions, origin, box):
    box = np.array(box)
    p = np.copy(positions)
    p -= origin
    return shift_pbc(p,box)[0]

