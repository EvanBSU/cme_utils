"""Module for mapping coarse models from fine models."""
import numpy
import itertools
from . import ff
from . import hoomd_xml
from .utilities import rotation_matrix_from_to
from .utilities import shift_indicies_in_list
from .utilities import alphabetize
from .utilities import remove_items_not_containing
from .utilities import remove_items_containing
from .model_handler import ModelHandler

def S_ring(nlist,types):
    ring=[-1 for t in types]
    for i,t in enumerate(types):
        if t=='S':
            rid = max(ring)+1
            loop = [i]+nlist[i]
            second_neighbors = nlist[loop[-2]]+nlist[loop[-1]]
            for c in itertools.combinations(second_neighbors,2):
                if c[0] in nlist[c[1]]:
                    loop+=[c[0],c[1]]
                    break
            for l in loop:
                ring[l]=rid
    return ring

def OA(nlist,types):
    ogroup=[-1 for t in types]
    for i,t in enumerate(types):
        if t=='O':
            oid = max(ogroup)+1
            ogroup[i]=oid
            for n in nlist[i]:
                if types[n]=='CA':
                    ogroup[n] =oid
    return ogroup

def NA(nlist,types):
    ngroup=[-1 for t in types]
    for i,t in enumerate(types):
        if t=='N':
            nid = max(ngroup)+1
            ngroup[i]=nid
            for n in nlist[i]:
                if types[n]=='CT':
                    ngroup[n] =nid
                    break
    return ngroup

def cn(i,blob,nlist,types,depthmax):
    blob.append(i)
    if len(blob) == depthmax:
        return blob
    for n in nlist[blob[-1]]:
        if types[n] == 'CT' and not n in blob:
            return cn(n,blob,nlist,types,depthmax)

def crawl(i,nlist,types,dep,depth):
    dep[i]=depth
    for n in nlist[i]:
        if types[n]=='CT' and dep[n]==-1:
            dep =  crawl(n,nlist,types,dep,depth+1)
    return dep

def C3(nlist,types,gobble=3):
    #very BDT-TPD specific right now
    #label each particle with depth, start at highest depth and take gobble amount
    ct = []
    dep = [-1 for t in types]
    for i,t in enumerate(types):
        if t=='CT':
            for n in nlist[i]:
                if types[n] in ['O','N']: 
                    dep = crawl(i,nlist,types,dep,0) #find a CT near an O or N and crawl it
    cgroup = [-1 for t in types]

    md = max(dep)
    for i,d in enumerate(dep):
        if d == md:
            cid = max(cgroup)+1
            blob = []
            blob = cn(i,blob,nlist,types,gobble)
            for b in blob:
                cgroup[b] = cid
                dep[b]=-1
    md = max(dep)
    for i,d in enumerate(dep):
        if d == md:
            cid = max(cgroup)+1
            blob = []
            blob = cn(i,blob,nlist,types,gobble)
            for b in blob:
                cgroup[b] = cid
                dep[b]=-1
    md = max(dep)
    for i,d in enumerate(dep):
        if d == md:
            cid = max(cgroup)+1
            blob = []
            blob = cn(i,blob,nlist,types,gobble)
            for b in blob:
                cgroup[b] = cid
                dep[b]=-1
    return cgroup

    #maxd = -1
    ##TODO: The following may be BDT-specific
    #for c in ct:
    #    if c != None:
    #        if c[2]>maxd:
    #            maxd = c[2]
    #for i,c in enumerate(ct):
    #    if c !=None:
    #        if c[2] == maxd:
    #            cid = max(cgroup)+1
    #            blob = cn([i],nlist,types,1,gobble)
    #            for b in blob:
    #                cgroup[b] = cid
    #                ct[b] = None
    #return cgroup
#TODO:map a whole configuration

#TODO:following code used to be inside builder.
    def load_mapping(self,filename="mapping.txt"):
        a = numpy.loadtxt(filename)

    def write_mapping(self,mping,filename="mapping.txt"):
        with open(filename,'w') as f:
            for i,m in enumerate(mping):
                for n in m[4]:
                    f.write("{:d} {:d}\n".format(n,i))

    def map_aa_to_ua(self):
        tm = {'CS':'CA','CT':'CT','CB':'CA','OS':'O','C!':'C!','CA':'CA','CW':'CA','S':'S','NS':'N','CP':'CA','O':'O'}
        self.aa2ua = []
        self.populate_neighborlist()
        uaid=0
        for i,t in enumerate(self.props['type']): #loop over all particles
            if not t in ['HC','HA']:
                mass = self.props['mass'][i]
                com = self.props['position'][i]*mass
                nbers = [i]
                for j in self.neighbors[i]:
                    if self.props['type'][j] in ['HA','HC']:
                        mass += self.props['mass'][j]
                        com += self.props['mass'][j]*self.props['position'][j]
                        nbers.append(j)
                com/=mass
                self.aa2ua.append( (tm[t],mass,com,i,tuple(nbers) ))
                uaid +=1
        bb=building_block()
        xyz = []
        for i,p in enumerate(self.aa2ua):
            bb.props['type'].append(p[0])
            bb.props['mass'].append(p[1])
            for j in self.neighbors[p[3]]:
                if not self.props['type'][j] in ['HA','HC']:
                    for k,pp in enumerate(self.aa2ua):
                        if pp[3]==j:
                            if k>i:
                                b = ['bond',str(i),str(k)]
                            else:
                                b = ['bond',str(k),str(i)]
                            if not b in bb.props['bond']:
                                bb.props['bond'].append(b)
            xyz.append(p[2])
        bb.props['position'] = numpy.array(xyz)
        self.write_mapping(self.aa2ua,"ua.txt")
        bb.write("ua.xml")

    def map_ua_to_cg(self,filename="cg.txt"):
        self.aa2ua = []
        self.populate_neighborlist()
        cg =S_ring(self.neighbors,self.props['type'])
        ogroups =OA(self.neighbors,self.props['type'])
        m = max(cg)
        for i in range(len(ogroups)):
            if ogroups[i]!=-1:
                cg[i]=m+ogroups[i]+1
        ngroups =NA(self.neighbors,self.props['type'])
        m = max(cg)
        for i in range(len(ngroups)):
            if ngroups[i]!=-1:
                cg[i]=m+ngroups[i]+1
        cgroups =C3(self.neighbors,self.props['type'],3)
        m = max(cg)
        for i in range(len(cgroups)):
            if cgroups[i]!=-1:
                cg[i]=m+cgroups[i]+1
        with open(filename,'w') as f:
            for i,s in enumerate(cg):
                if s !=-1:
                    f.write("{} {}\n".format(i,s))
        ids = list(set(cg))
        cg_beads = []
        for c in ids:
            if c !=-1:
                blob=[i for i,x in enumerate(cg) if x==c]
                t = 'CT'
                if 'S' in [self.props['type'][b] for b in blob]:
                    t = 'S'
                elif 'O' in [self.props['type'][b] for b in blob]:
                    t = 'O'
                elif 'N' in [self.props['type'][b] for b in blob]:
                    t = 'N'
                elif 'CA' in [self.props['type'][b] for b in blob]:
                    t = 'CA'
                mass = 0.
                com = numpy.array([0.,0.,0.])
                for b in blob:
                    mass += float(self.props['mass'][b])
                    com += self.props['mass'][b]*self.props['position'][b]
                com/=mass
                cg_beads.append((t,mass,com,c,tuple(blob)))
                #TODO:Bonds

        bb=building_block()
        xyz = []
        for i,p in enumerate(cg_beads):
            bb.props['type'].append(p[0])
            bb.props['mass'].append(p[1])
            xyz.append(p[2])
        bb.props['position'] = numpy.array(xyz)
        self.write_mapping(cg_beads,"cg.txt")
        bb.write("cg.xml")
