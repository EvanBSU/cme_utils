import signac

def difference(a,b):
    nSp_a = len(a.statepoint())
    nSp_b = len(b.statepoint())
    if nSp_a==nSp_b:
        for key in a.statepoint():
            if b.statepoint()[key] != a.statepoint()[key]:
                yield key,a,a.statepoint()[key],b,b.statepoint()[key]
    elif nSp_a>nSp_b:
        for key in a.statepoint():
            if key in b.statepoint():
                if b.statepoint()[key] != a.statepoint()[key]:
                    yield key,a,a.statepoint()[key],b,b.statepoint()[key]
            else:
                yield key,a,a.statepoint()[key],b,None
    else:
        for key in b.statepoint():
            if key in a.statepoint():
                if b.statepoint()[key] != a.statepoint()[key]:
                    yield key,a,a.statepoint()[key],b,b.statepoint()[key]
            else:
                yield key,a,None,b,b.statepoint()[key]

def diff_jobs(workspace_path,signac_id1,signac_id2):
    project = signac.get_project(workspace_path)
    job1 = project.open_job(id=signac_id1)
    job2 = project.open_job(id=signac_id2)
    diff_keys = difference(job1,job2)
    for key,j1,v1,j2,v2 in diff_keys:
        print(key,'\n',j1,':',v1,'\n',j2,':',v2)
