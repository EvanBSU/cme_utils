from cme_utils.job_control import manager
import sys
import os
if __name__=='__main__':
    home_dir = os.environ['HOME']
    if sys.argv[1] == 'mav':
        db = home_dir+'/sim_db.sqlite3'
    else:
        db = 'sim_db.sqlite3'
    d = manager.db_manager(db)
    d.update(sys.argv[2],sys.argv[3],sys.argv[4])
